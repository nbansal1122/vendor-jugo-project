import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TabModule } from 'angular-tabs-component';
import { BarRatingModule } from "ngx-bar-rating";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { CommonService } from './framework/common.service';
import { Http, HttpModule } from '@angular/http';
import { CommonDataService } from './shared/sharedDataService';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MakepaymentComponent } from './makepayment/makepayment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MakepaymentModule } from './makepayment/makepayment.module';
import { ForgetComponent } from './layout/forget/forget.component';
import { ResetComponent } from './layout/reset/reset.component';


// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        BarRatingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        TabModule,
        AppRoutingModule,
        MakepaymentModule,
        NgbModule.forRoot()
    ],
    declarations: [AppComponent, ForgetComponent, ResetComponent],

    exports: [
    ],

    providers: [AuthGuard, CommonService, CommonDataService],
    bootstrap: [AppComponent],
})
export class AppModule {}
