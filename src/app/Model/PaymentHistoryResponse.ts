import { DateUtil } from './../framework/Utils/DateUtil';
import { Vendor } from './Loginrequest/LoginRequest';
import { Type } from 'class-transformer';
import { BaseResponse } from './../framework/BaseResponseModel';
import { AppUtil } from '../framework/Utils/AppUtil';

export class Category {
    categoryName: string;
    url: string;
    description: string;
    categoryId: number;
}

export class Subscriptions {
    validity: string;
    days: number;
    subscriptionId: number;
}

export class SubscriptionPackage {
    name: string;
    subscriptionId: number;
    amount: number;
    categoryId: number;
    isActive: number;
    paymentPackageId: number;
    @Type(() => Category)
    category: Category;
    @Type(() => Subscriptions)
    subscriptions: Subscriptions;
}

export class PaymentInfo {
    vendorId: number;
    @Type(() => Vendor)
    vendor: Vendor;
    @Type(() => SubscriptionPackage)
    subscriptionPackage: SubscriptionPackage;
    expiryDate: string;
    amount: number;
    subscriptionTransactionId: number;
    status: string;
    paymentDate: string;
    paymentMode: string;

    public get getPaymentDate() {
        if (this.paymentDate) {
            return DateUtil.getStandardTime(this.paymentDate, "DD-MM-YYYY HH:mm:ss", "DD-MM-YYYY");
        } else if (this.vendor && this.vendor.updated){
        return DateUtil.getStandardTime(this.vendor.updated,"DD-MM-YYYY HH:mm:ss", "DD-MM-YYYY");
        }
         return '';
    }

    public get getPaymentStatus() {
        if (this.status === PaymentStatusEnum.Credit ) {
            return 'Credited';
        } else if (this.status === PaymentStatusEnum.Pending) {
            return 'Pending';
        } else if (this.status === PaymentStatusEnum.Failed) {
            return 'Rejected';
        } else if (this.status === PaymentStatusEnum.Initiated) {
            return 'Initiated';
        }
        return '';
    }



}

export class PaymentHistoryResponse extends BaseResponse {
    @Type(() => PaymentInfo)
    data: PaymentInfo[];
}

export class PaymentTransactionResponse extends BaseResponse {
    @Type(() => PaymentInfo)
    data: PaymentInfo;
}



export enum PaymentStatusEnum {
Credit = 'Credit',
Pending = 'Pending',
Failed = 'Failed',
Initiated = 'Initiated'
}
