import { BaseResponse } from "../../framework/BaseResponseModel";
import { Type } from "class-transformer";

export class CategoryData {
  categoryName: string;
  url: string;
  description: string;
  categoryId: number;
  }
  export class Categorydataresponse  extends BaseResponse{
    @Type(() => CategoryData)
    data:CategoryData;


  }
   