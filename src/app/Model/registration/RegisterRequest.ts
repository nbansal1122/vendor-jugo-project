import { BaseComponent } from "../../framework/BaseCompo";
import { Type } from "class-transformer";
import { BaseResponse } from "../../framework/BaseResponseModel";

export class RegisterRequest {
    firstName: string;
    lastName: string;
    contactNumber: string;
    email: string;
    username: string;
    alternateContact: string;
    password: string;
    businessName: string;
    address:string;
    cityId:number
    tagline:string;
    aboutText:string;
    websiteurl:string;
    vendorId: number;
    @Type(() => Categories)
    categories:Categories;

    // @Type(() => Address)
    // add:Address;

    // address:string = this.add.addressline1 + this.add.addressline2 + this.add.landmark + this.add.locality + this.add.city + this.add.pincode;

    public get firstNameError() {
        if (this.firstName) {
            return true;
        }
        return false;
    }
    public get lastNameError() {
        if (this.lastName) {
            return true;
        }
        return false;
    } public get contactNumberError() {
        if (this.contactNumber) {
            return true;
        }
        return false;
    } public get emailError() {
        if (this.email) {
            return true;
        }
        return false;
    } public get usernameError() {
        if (this.username) {
            return true;
        }
        return false;
    } public get passwordError() {
        if (this.password) {
            return true;
        }
        return false;
    } public get businessNameError() {
        if (this.businessName) {
            return true;
        }
        return false;
    }
    // public get addressError() {
    //     if (this.address) {
    //         return true;
    //     }
    //     return false;
    // }


    public get isValid(){
        if(this.firstNameError || this.lastNameError || this.contactNumberError || this.usernameError || this.emailError || this.passwordError || this.businessNameError ){
            return true;
        }
        return false;
    }
  

}
export class Categories {
    cetegoryId: number[];
  }

// export class Address{
//     pincode:string;
//     addressline1:string;
//     addressline2:string;
//     locality:string;
//     landmark:string;
//     city:string;
// }

export class  RegisterResponse extends BaseResponse {
    @Type(() => RegisterRequest)
    data: RegisterRequest;

}