import { Type } from 'class-transformer';
import { BaseResponse } from './../framework/BaseResponseModel';
export class Category {
    categoryName: string;
    url: string;
    description: string;
    categoryId: number;
}

export class Subscriptions {
    validity: string;
    days: number;
    subscriptionId: number;
}

export class SubscriptionDetail {
    name: string;
    subscriptionId: number;
    amount: number;
    categoryId: number;
    isActive: number;
    paymentPackageId: number;
    @Type(() => Category)
    category: Category;
    @Type(() => Subscriptions)
    subscriptions: Subscriptions;
}

export class SubscriptionResponse extends BaseResponse {
    @Type(() => SubscriptionDetail)
    data: SubscriptionDetail;
}

