import { Type } from "class-transformer";
import { BaseResponse } from "../../framework/BaseResponseModel";
import { BaseComponent } from "../../framework/BaseCompo";
import { Url } from "url";

export class Category {
  categoryName: string;
  url: string;
  description: string;
  categoryId: number;
  active:boolean;

}

// export class Event{
//   eventTemplateName: string;
//     imageUrl: string;
//     colorCode: string;
//     eventTemplateId: number;
    
// }

export class Photo {
  iconUrl: string;
  vendorId: number;
  tag: string;
  caption: string;
  photoId: number;
}

export class VendorData {
  created: string;
  updated: string;
  status: string;
  id: number;
  role: string;
  email: string;
  token: string;
  lat: number;
  lng: number;
  address: string;
  firstName: string;
  lastName: string;
  username: string;
  contactNumber: string;
  alternateContact: string;
  businessName: string;
  displayCount: number;
  profileViewCount: number;
  emailContactCount: number;
  phoneContactCount: number;
  @Type(() => Category)
  categoryList: Category[];
  aboutText: string;
  tagline: string;
  averageRating: number;
  defaultRating: number;
  websiteUrl: string;
  @Type(() => Photo)
  photos: Photo[];
}

// export class EventResponse extends BaseResponse {
//   @Type(() => Event)
//   data: Event[];
// }


export class VendorResponse extends BaseResponse {
  @Type(() => VendorData)
  data: VendorData[];
}

export class VendorMasterResponse extends BaseResponse {
  @Type(() => VendorData)
  data: VendorData;
}
export class CategoryResponse extends BaseResponse {
  @Type(() => Category)
  data: Category[];
}

export class CategoryResponsebyid extends BaseResponse {
  @Type(() => Category)
  data: Category;
}
export class UplodePhotoResponse extends BaseResponse {
  @Type(() => ImageUrl)
  data: ImageUrl;
}

export class ImageUrl {
  url: string;
}

export class PhotoData {
  iconUrl: string;
  vendorId: number;
  tag: string = "tagline";
  caption: string = "caption";
}

export class VendorCategory{
  
    vendorId: number;
    cetegoryId: number[];
  
}

export class ResetVendorPassword{
  mobileNumber: string;
  newPassword: string;

}

export class CategoryStatusUpdate{
  id:number;
  status:boolean;
}