import { BaseResponse } from "../../framework/BaseResponseModel";
import { Type } from "class-transformer";

export class CityData {
    cityName: string;
    pincode: string;
    id: number;
    created: string;
    updated: string;
    active:boolean;
  }
  export class CityResponse  extends BaseResponse{
    @Type(() => CityData)
    data:CityData[];
  }

  export class CityDataResponse  extends BaseResponse{
    @Type(() => CityData)
    data:CityData;
  }

  export class CityStatusUpdate{
    id:number;
    status:boolean;
  }