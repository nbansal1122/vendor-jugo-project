import { Category } from './../vendor/vendor';
import { BaseResponse } from "../../framework/BaseResponseModel";
import { Type } from "class-transformer";
import { Categories } from "../registration/RegisterRequest";
import { CategoryData } from "../category/category";
 
export class  SubscriptionsData{
    validity: string;
    days: number;
    subscriptionId: number;
}

export class SubscriptionsResponse extends BaseResponse{
    @Type(()=> SubscriptionsData)
    data:SubscriptionsData[];

}
 
export class PackageData{
	name:string;
	subscriptionId:number;
	amount:number;
    categoryId:number;
    paymentPackageId: number;
    
}
export class PackageResponse extends BaseResponse{
    @Type(()=> PackageData)
    data:PackageData;
}

export class PackageDetail {
    name: string;
	subscriptionId: number;
	amount: number;
    categoryId: number;
    isActive: number;
    paymentPackageId: number;
    @Type(() => CategoryData)
    cat: CategoryData;
    @Type(() => SubscriptionsData)
    subscriptions: SubscriptionsData;
    @Type(() => Category)
    category: Category;
}

export class MakePayment {
        vendorId: number;
        paymentPackageId: number;
        online: boolean;
}


export class PackageDetailResponse extends BaseResponse{
    @Type(() => PackageDetail)
    data: PackageDetail[];
}
