import { BaseResponse } from './../../framework/BaseResponseModel';
import { Type } from "class-transformer";

export class EventData  {
    eventTemplateName: string;
    imageUrl: string;
    colorCode: string;
    eventTemplateId: number;
    @Type(() => DesignData)
    design: DesignData[];
  }
  export class EventDataResponse  extends BaseResponse{
    @Type(() => EventData)
    data:EventData[];
  }

  export class DesignData extends EventData {
    designImgUrl: string;
    parentEventTemplateId: number;
    designId: number;
  }
   
  export class DesignResponse extends BaseResponse{
    @Type(() => DesignData)
    data:DesignData[];
  }

 export class DesignDataResponse extends BaseResponse{
    @Type(() => DesignData)
    data:DesignData;
  }
  export class UserData {
    eventTitle: string;
    dateTime: number;
    location: string;
    link: string;
    designId?: number;
    description: string;
    media: string;
    venue?: any;
    homeAddress?: any;
    homeAddressLink?: any;
    eventId: number;
    userId: number;
    wishListIds: string[];
    itemIdVsPurchasedBy?: any;
    @Type(() => DesignData)
    design: DesignData[];
    @Type(() => EventData )
    eventTemplate: EventData[];
    isbankDetailsPresent: boolean;
}

export class UserResponse extends BaseResponse{
  @Type(() => UserData)
  data:UserData;
}

export class Images{
  imageurl:string;
}

export class ImageResponse extends BaseResponse{
  @Type(() => Images)
  data: Images;
}