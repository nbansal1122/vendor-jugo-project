export class AppUtil {
    static isNullOrEmptyString(T: string) {
        return T === undefined || T === null || T === ''
    }

    static isNullEmpty(data: any) {
        return data === undefined || data === null
    }

    static isListNullOrEmpty(list: any[]) {
        return list === undefined || list === null || list.length === 0
    }

    static isEmpty(list: any[]) {
        return list === null || list=== undefined || list.length === 0;
    }
}
