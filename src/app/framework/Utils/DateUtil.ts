import * as moment from 'moment';




export class DateUtil {

  static getStandardTime(serverdatestr: string, serverformat: string, reqformat: string) {
    // return moment(moment(serverdatestr, serverformat).toDate()).format(reqformat);
    return moment(moment.utc(serverdatestr, serverformat).toDate()).format(reqformat);

  }

  static getStringDatefromJSON(json: any, reqformat: string) {

    return moment(moment(json).toISOString()).format(reqformat);
  }


  static getStringDatefromDate(date: Date, dateFormat: string) {

    return moment(moment(date)).format(dateFormat);

  }

  static getDatefromString(datestring: string, format: string) {
    return moment(datestring, format).toDate();
  }

  static isValid(date: Date) {
    return moment(date).isValid();
  }


  static getDatefromMs(milliseconds: number, reqdateformat: string) {
    return moment(milliseconds).format(reqdateformat);
  }


  static getLocalDatefromServer(serverDate:string, format: string, reqFormat: string){
    moment(moment(serverDate, format).add(330,'minutes').toDate()).format(reqFormat);
      return   moment(moment(serverDate, format).add(330,'minutes').toDate()).format(reqFormat);
  }

  static getTimeStamp(date:Date){
    console.log("timestamp is"+moment(date).format('X'));
    return moment(date).format('X');
  }
}

