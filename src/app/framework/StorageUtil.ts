import { classToPlain, plainToClass } from "class-transformer";
import { JsonParser } from "./ApiGenerator";


export class StorageUtil {

    static getAuthToken() {
        return this.getItemLocal(StorageKeysEnum.TOKEN);
    }

    static saveToken(token: string) {
        this.setItemLocal(StorageKeysEnum.TOKEN, token);
    }

    static getUserId() {
        //TODO: remove hardcoded value = 1
        // return this.getItem(KEYS.USER_ID);
        return '1';
    }

    static logoutUser() {
        // logoutuser
        // localStorage.removeItem(KEYS.USER_DATA);
        this.clearAllData();
    }

    static clearAllData() {
        localStorage.clear();
        sessionStorage.clear();
    }

    static setItemLocal(key: string, value: string) {
        localStorage.setItem(key, value)
    }
    static getItemLocal(key: string) {
        return localStorage.getItem(key)
    }


    static setItemGlobal(key: string, value: string) {
        sessionStorage.setItem(key, value)
    }
    static getItemGlobal(key: string) {
        return sessionStorage.getItem(key)
    }

    static getRole() {
        return this.getItemLocal("role");
    }

    static saveRole(role: string) {
        this.setItemLocal("role", role);
    }

    static getId() {
        return this.getItemLocal(StorageKeysEnum.VENDOR_ID);
    }

    static saveId(id: string) {
        this.setItemLocal(StorageKeysEnum.VENDOR_ID, id);
    }

    static getPendingPayment() {
        return this.getItemLocal("pendingPayment") == "true" ;
    }

    static savePendingPayment(payment:boolean) {
        this.setItemLocal("pendingPayment", payment+"");
    }

    static getUserRole() {
        return this.getItemLocal(StorageKeysEnum.VENDOR_ROLE);
    }

    static saveUserRole(role: string) {
        this.setItemLocal(StorageKeysEnum.VENDOR_ROLE, role);
    }

    static getEventId() {
        return this.getItemLocal(StorageKeysEnum.EVENT_ID);
    }

    static saveEventId(id: string) {
        this.setItemLocal(StorageKeysEnum.EVENT_ID, id);
    }

    // static authToken = StorageUtil.getAuthToken();
}

export enum StorageKeysEnum {
    TOKEN = "token",
    USER_ID = "userId",
    VENDOR_ID = "vendorId",
    VENDOR_ROLE ="vendorRole",
    EVENT_ID = "eventId"

}
