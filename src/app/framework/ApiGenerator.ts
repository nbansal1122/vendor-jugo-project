import { EventDataResponse, EventData, DesignResponse, UserData, UserResponse, ImageResponse, Images, DesignData } from './../Model/event/event';


import { PaymentHistoryResponse } from './../Model/PaymentHistoryResponse';
import { PaymentResponse } from './../Model/PaymentResponse';
import { SubscriptionResponse } from './../Model/SubscriptionResponse';
import { Login, LoginResponse, ForgotPasswordResponse, ForgotPassword } from './../Model/Loginrequest/LoginRequest';
import { HttpRequest, HttpGenericRequest } from './HttpRequest';
import * as global from './globals';
import { classToPlain, plainToClass } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';
import { TaskCode, GET_PAYMENT_HISTORY, CONFIRM_PENDING_PAYMENT } from './globals';
import { RegisterRequest } from '../Model/registration/RegisterRequest';
import {
  VendorResponse, UplodePhotoResponse, PhotoData, VendorMasterResponse, VendorData,
  VendorCategory, CategoryResponse, Category, Photo, ResetVendorPassword, CategoryStatusUpdate} from '../Model/vendor/vendor';
import { CountResponse, CountType, UserCount } from '../Model/userView/vendorCount';
import { Categorydataresponse, CategoryData } from '../Model/category/category';
import {
  SubscriptionsResponse, PackageData, PackageResponse, PackageDetailResponse, PackageDetail,
  MakePayment
} from '../Model/Package/package';
import { CityResponse, CityData, CityDataResponse, CityStatusUpdate } from '../Model/city/city';
import { StorageUtil } from './StorageUtil';
import { BaseResponse } from './BaseResponseModel';



export class ApiGenerator {
  static authTokenForEvent="42c0f8d2-5632-41b7-a41e-7e5d6dc5f76b";

  static postRegisterRequest(register: RegisterRequest) {
    console.log('Register request :: ' + JSON.stringify(register));
    const http = new HttpRequest(global.SIGN_UP);
    http.setPostMethod();
    http.params = classToPlain(register);
    http.removeDefaultHeaders();
    http.classTypeValue = RegisterRequest;
    http.taskCode = TaskCode.SIGN_UP;
    return http;
  }

  static postLogin(loginUser: Login) {
    const http = new HttpRequest(global.LOGIN);
    http.addDefaultHeaders();
    http.setPostMethod();
    http.params = classToPlain(loginUser);
    http.taskCode = TaskCode.LOGIN;
    http.classTypeValue = LoginResponse;
    return http;
  }

  static postForgetPassword(forgotPassword: ForgotPassword) {
    const http = new HttpRequest(global.FORGOT_PASSWORD);
    http.setPostMethod();
    http.params = classToPlain(forgotPassword);
    http.taskCode = TaskCode.FORGOT_PASSWORD;
    http.classTypeValue = ForgotPasswordResponse;
    return http;

  }

  static getUserId(userId: number) {
  // let token = '424b38ed-adfc-4c97-bf92-8290dd99effb';
    const http = new HttpRequest(global.USER.concat(userId.toString()));
    http.classTypeValue = VendorMasterResponse;
    // http.addHeaders('authToken',token);
    http.taskCode = TaskCode.USER;
    return http;
  }

  static getAllVendor() {
    const http = new HttpRequest(global.GET_ALL_VENDOR);
    http.classTypeValue = VendorResponse;
    http.taskCode = TaskCode.GET_ALL_VENDOR;
    return http;
  }

  static resetVendorPassword(resetVendor: ResetVendorPassword) {
    const http = new HttpRequest(global.RESET_PASSWORD);
    http.addDefaultHeaders();
    http.setPostMethod();
    http.params = classToPlain(resetVendor);
    http.taskCode = TaskCode.RESET_PASSWORD;
    http.classTypeValue = VendorResponse;
    return http;
  }

  static updateStatus(item: VendorData) {
    console.log('update status ::' + item)
    const http = new HttpRequest(global.UPDATE_STATUS + item.id + '?status=' + item.status);
    http.removeDefaultHeaders();
    http.setPatchMethod();
    http.params = classToPlain(item as object);
    http.classTypeValue = VendorResponse;
    http.taskCode = TaskCode.UPDATE_STATUS;
    return http;
  }

  static updateProfile(vendor: VendorData) {
    const http = new HttpRequest(global.UPDATE_PROFILE);
    http.setPatchMethod();
    http.params = classToPlain(vendor as object);
    http.removeDefaultHeaders();
    http.classTypeValue = VendorMasterResponse;
    http.taskCode = TaskCode.UPDATE_PROFILE;
    return http;
  }

  static getFileRequest(file: File) {
    var data: FormData = new FormData();
    data.append('file', file, file.name);
    data.append('filetype', '1');
    return this.uploadFile(data);
  }

  static uploadFile(data: FormData) {
    const http = new HttpRequest(global.UPLOAD_PHOTO);
    http.taskCode = TaskCode.UPLOAD_PHOTO;
    http.removeDefaultHeaders();
    http.classTypeValue = UplodePhotoResponse;
    http.setPostMethod();
    http.params = data;
    return http;

  }

  static uploadImage(photo: PhotoData) {
    console.log('upload image api' + photo);
    const http = new HttpRequest(global.UPLOAD_IMAGE);
    http.setPostMethod();
    http.params = classToPlain(photo);
    http.removeDefaultHeaders();
    http.classTypeValue = VendorResponse;
    http.taskCode = TaskCode.UPLOAD_IMAGE;
    return http;
  }

  static getCountType(vendorCount: CountType, pageNo: number, size: number) {
    const http = new HttpRequest(global.GET_COUNTTYPE + vendorCount.id + '?countType=' + vendorCount.countType + '&page=' + pageNo
      + '&items_per_page=' + size);
    http.taskCode = TaskCode.GET_COUNTTYPE;
    http.classTypeValue = CountResponse;
    return http;
  }

  static getAllCategory() {
    const http = new HttpRequest(global.GET_ALL_CATEGORY);
    http.classTypeValue = CategoryResponse;
    http.taskCode = TaskCode.GET_ALL_CATEGORY;
    return http;
  }

  static addCategory(cat: VendorCategory) {
    const http = new HttpRequest(global.ADD_CATEGORY);
    http.addDefaultHeaders();
    http.setPostMethod();
    http.params = classToPlain(cat);
    http.taskCode = TaskCode.ADD_CATEGORY;
    http.classTypeValue = CategoryResponse;
    return http;
  }

  static addCategorydata(cat: CategoryData) {
    const http = new HttpRequest(global.ADD_ALL_CATEGORY);
    http.addDefaultHeaders();
    http.setPostMethod();
    http.params = classToPlain(cat as Object);
    http.taskCode = TaskCode.ADD_ALL_CATEGORY;
    http.classTypeValue = Categorydataresponse;
    return http;
  }

  static updateCategorydata(cat: CategoryData, catId: Category) {
    const http = new HttpRequest(global.UPDATE_CATEGORY + catId.categoryId);
    http.addDefaultHeaders();
    http.setPatchMethod();
    http.params = classToPlain(cat as Object);
    http.taskCode = TaskCode.UPDATE_CATEGORY;
    http.classTypeValue = Categorydataresponse;
    return http;
  }

  static getCategory(catId: Category) {
    const http = new HttpRequest(global.GET_ALL_CATEGORY_BY_VENDOR_ID);
    http.classTypeValue = CategoryResponse;
    http.taskCode = TaskCode.GET_ALL_CATEGORY_BY_VENDOR_ID;
    return http;
  }

  static updateCategoryStatus(statusUpdate:CategoryStatusUpdate){
    const http = new HttpRequest(global.UPDATE_CATEGORY_STATUS);
    http.setPatchMethod();
    http.params = classToPlain(statusUpdate as Object);
    http.taskCode = TaskCode.UPDATE_CATEGORY_STATUS;
    http.classTypeValue = CategoryStatusUpdate;
    return http;
  }

  static deleteCategory(category: Category) {
    const http = new HttpRequest(global.DELETE_CATEGORY + category.categoryId);
    http.setDeleteMethod();
    http.taskCode = TaskCode.DELETE_CATEGORY;
    return http;
  }


  ///////////////////// city//

  static getAllCity() {
    const http = new HttpRequest(global.GET_ALL_CITY);
    http.classTypeValue = CityResponse;
    http.taskCode = TaskCode.GET_ALL_CITY;
    return http;
  }


  static updateCitydata(city: CityData, cityId: CityData) {
    const http = new HttpRequest(global.UPDATE_CITY);
    console.log(global.UPDATE_CITY + cityId.id);
    http.addDefaultHeaders();
    http.setPatchMethod();
    http.params = classToPlain(city as Object);
    http.taskCode = TaskCode.UPDATE_CITY;
    http.classTypeValue = CityDataResponse;
    return http;
  }


  static addNewCity(city: CityData) {
    const http = new HttpRequest(global.ADD_CITY);
    http.addDefaultHeaders();
    http.setPostMethod();
    http.params = classToPlain(city as Object);
    http.taskCode = TaskCode.ADD_CITY;
    http.classTypeValue = CityDataResponse;
    return http;
  }

  static updateCityStatus(statusUpdate:CityStatusUpdate){
    const http = new HttpRequest(global.UPDATE_CITY_STATUS);
    http.setPatchMethod();
    http.params = classToPlain(statusUpdate as Object);
    http.taskCode = TaskCode.UPDATE_CITY_STATUS;
    http.classTypeValue = CategoryStatusUpdate;
    return http;
  }


  /////////////////////////
  static deleteVendorImage(photo: Photo) {
    const http = new HttpRequest(global.DELETE_VENDOR_IMAGE + photo.photoId);
    http.setDeleteMethod();
    http.taskCode = TaskCode.DELETE_VENDOR_IMAGE;
    return http;
  }

  static getSubscritption() {
    const http = new HttpRequest(global.GET_SUBSCRIPTION);
    http.classTypeValue = SubscriptionsResponse;
    http.taskCode = TaskCode.GET_SUBSCRIPTION;
    return http;
  }
  static addPackage(packagedata: PackageData) {
    const http = new HttpRequest(global.ADD_ALL_PACKAGE);
    http.addDefaultHeaders();
    http.setPostMethod();
    http.params = classToPlain(packagedata as Object);
    http.taskCode = TaskCode.ADD_ALL_PACKAGE;
    http.classTypeValue = PackageResponse;
    return http;
  }

  static updatePackage(packagedata: PackageData , packageId: PackageData){
    const http = new HttpRequest(global.UPDATE_PACKAGE);
    http.addDefaultHeaders();
    http.setPatchMethod();
    http.params = classToPlain(packagedata as Object);
    http.taskCode = TaskCode.UPDATE_PACKAGE;
    http.classTypeValue = PackageResponse;
    return http;


  }



  static getPackage() {
    const http = new HttpRequest(global.ADD_ALL_PACKAGE);
    http.classTypeValue = PackageDetailResponse;
    http.taskCode = TaskCode.ADD_ALL_PACKAGE;
    return http;
  }
  static deletePackage(packagedata: PackageDetail) {
    const http = new HttpRequest(global.DELETE_PACKAGE + packagedata.paymentPackageId);
    // http.addDefaultHeaders();
    http.setDeleteMethod();
    // http.params = classToPlain(packagedata as Object);
    http.taskCode = TaskCode.DELETE_PACKAGE;
    // http.classTypeValue = ;
    return http;
  }

  static makePayment(payment: MakePayment) {
    const http = new HttpRequest(global.MAKE_PAYMENT);
    http.setPostMethod();
    http.params = classToPlain(payment as object);
    http.taskCode = TaskCode.MAKE_PAYMENT;
    http.classTypeValue = PaymentResponse;
    return http;
  }


  static getMaxValueSubscription(vendorId: number, subscriptionId: number) {
    const http = new HttpRequest(global.GET_MAX_VALUE_VENDOR_SUBSCRIPTION);
    http.addQueryParam('vendorId', vendorId.toString());
    http.addQueryParam('subscriptionId', subscriptionId.toString());
    http.getCompleteUrl();
    http.taskCode = TaskCode.GET_MAX_VALUE_VENDOR_SUBSCRIPTION;
    http.classTypeValue = SubscriptionResponse;
    return http;
  }



  static getPaymentHistory(status: string) {
    const http = new HttpRequest(global.GET_PAYMENT_HISTORY);
    http.addQueryParam('status', status);
    http.getCompleteUrl();
    http.taskCode = TaskCode.GET_PAYMENT_HISTORY;
    http.classTypeValue = PaymentHistoryResponse;
    return http;
  }



  static getPaymentHistorybyVendorId(vendorId: number) {
    const http = new HttpRequest(global.GET_PAYMENT_HISTORY);
    http.addQueryParam('vendorId', vendorId.toString());
    http.getCompleteUrl();
    http.taskCode = TaskCode.GET_PAYMENT_HISTORY;
    http.classTypeValue = PaymentHistoryResponse;
    return http;
  }



  static confirmPendingPayment(subscriptionId: number) {
    const http = new HttpRequest(global.CONFIRM_PENDING_PAYMENT.concat(subscriptionId.toString()));
    http.removeDefaultHeaders();
    http.taskCode = TaskCode.CONFIRM_PENDING_PAYMENT;
    return http;
  }

  static rejectPendingPayment(subscriptionId: number) {
    const http = new HttpRequest(global.REJECT_PENDING_PAYMENT.concat(subscriptionId.toString()));
    http.removeDefaultHeaders();
    http.taskCode = TaskCode.REJECT_PENDING_PAYMENT;
    return http;
  }



  static verifyOnlinePayment(transacNumber: string) {
    const http = new HttpRequest(global.VERIFY_ONLINE_PAYMENT);
    http.removeDefaultHeaders();
    http.addQueryParam('transactionNumber', transacNumber.toString());
    http.getCompleteUrl();
    http.taskCode = TaskCode.VERIFY_ONLINE_PAYMENT;
    return http;
  }
/////////////////////////////

    static getAllEvent(){
      const http = new HttpRequest(global.GET_ALL_EVENT_TEMPLATE);
    http.classTypeValue = EventDataResponse;
    http.addHeaders('authToken',this.authTokenForEvent);
    http.taskCode = TaskCode.GET_ALL_EVENT_TEMPLATE;
    return http;
    }

    static getEventById(eventTemplateId: string){
      const http = new HttpRequest(global.GET_EVENT_BY_ID + eventTemplateId);
      http.classTypeValue = DesignResponse;
      http.taskCode = TaskCode.GET_EVENT_BY_ID;
      http.addHeaders('authToken',this.authTokenForEvent); 
      return http;
    }

    static deleteEventTemplate(eventId: number) {
      const http = new HttpRequest(global.DELETE_EVENT_TEMPLATE +eventId+"");
      http.addHeaders('authToken', this.authTokenForEvent)
      http.addHeaders('Content-Type', 'application/json')
      http.setDeleteMethod();
      http.taskCode = TaskCode.DELETE_EVENT_TEMPLATE;
      http.classTypeValue = BaseResponse;
      return http;
    }

    static addUserData(user: UserData) {
      const http = new HttpRequest(global.CREATE_EVENT);
      http.addDefaultHeaders();
      http.setPostMethod();
      http.params = classToPlain(user as Object);
      http.taskCode = TaskCode.CREATE_EVENT;
      http.addHeaders('authToken',this.authTokenForEvent); 
      http.classTypeValue = UserResponse;
      return http;
    }

    
    static getFilesRequests(file: File) {
      var data: FormData = new FormData();
      data.append('file', file, file.name);
      data.append('filetype', '1');
      return this.addPhoto(data);
    }
  
    static addPhoto(data: FormData) {
      const http = new HttpRequest(global.ADD_IMAGE);
      http.taskCode = TaskCode.ADD_IMAGE;
      http.addHeaders('authToken',this.authTokenForEvent); 
      http.removeDefaultHeaders();
      http.classTypeValue = ImageResponse;
      http.setPostMethod();
      http.params = data;
      return http;
    }

    static saveImage(photo:DesignData) {
      console.log('upload image api' + photo);
      const http = new HttpRequest(global.SAVE_IMAGE);
      http.addHeaders('authToken',this.authTokenForEvent); 
      http.setPostMethod();
      http.params = classToPlain(photo);
      http.classTypeValue = DesignResponse;
      http.taskCode = TaskCode.SAVE_IMAGE;
      return http;
    }

    static updateImage(photo: DesignData, photoId: DesignData){

    }

    static deleteImage(photo: DesignData) {
      const http = new HttpRequest(global.DELETE_DESIGN_IMAGE + photo.designId);
      http.addHeaders('authToken', this.authTokenForEvent)
      http.addHeaders('Content-Type', 'application/json')
      http.setDeleteMethod();
      http.taskCode = TaskCode.DELETE_DESIGN_IMAGE;
      return http;
    }
  
  
  
  
  
}



export class JsonParser {
  static parseJson<T>(response: any, type: ClassType<T>): T {
    const parsedResponse = plainToClass(type, response as object);
    return parsedResponse;
  }

  static parseJsonString(response: any, type: ClassType<any>): any {
    const parsedResponse = plainToClass(type, response as object);
    return parsedResponse;
  }

  static parseJsonArray(response: any, type: ClassType<any>): any {
    const parsedResponse = plainToClass(type, response);
    return parsedResponse;
  }
}
