import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule,MatChipsModule, MatIconModule, MatFormFieldModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import { Http } from '@angular/http';
import { MapsAPILoader, AgmCoreModule } from '@agm/core';
import { HttpClient } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    MatChipsModule,
    MatIconModule, MatIconModule, MatAutocompleteModule, MatFormFieldModule,
    SignupRoutingModule,
    FormsModule,ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBblTka0hxpRRDaLsLemtmtbPyp0j5FeAI',
      // AIzaSyBblTka0hxpRRDaLsLemtmtbPyp0j5FeAI
      // AIzaSyCyZpxf2q2UMCgiGVmGrBjNJT1g3-NiBdc
      // AIzaSyC0zHzGrZBXFzsvfIxDgDNde-QjHNTpLQc    tested not working
      // AIzaSyDZq8zgQxsVL1owtPhkLkjfH3s1EiIEWIA    tested not working
      libraries: ['places']
      }),
  ],
  providers: [],
  declarations: [SignupComponent]
})
export class SignupModule { }
