import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { MakepaymentComponent } from './makepayment/makepayment.component';
import { ResetComponent } from './layout/reset/reset.component';
import { ForgetComponent } from './layout/forget/forget.component';

const routes: Routes = [
    
    { path: '', loadChildren: './layout/layout.module#LayoutModule', canActivate: [AuthGuard] },
    { path: 'login', loadChildren: './login/login.module#LoginModule'},
    { path: 'makepayment', loadChildren:'./makepayment/makepayment.module#MakepaymentModule'},
    { path: 'payment-success', loadChildren: './payment-success/payment-success.module#PaymentSuccessModule' },
    { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path: 'reset-password', component: ResetComponent},
    { path: 'forget-password', component: ForgetComponent},
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
