import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MakepaymentRoutingModule } from './makepayment-routing.module';
import { MakepaymentComponent } from './makepayment.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MakepaymentRoutingModule,
    FormsModule
  ],
  declarations: [MakepaymentComponent],
  exports: [MakepaymentComponent]
})
export class MakepaymentModule { }
