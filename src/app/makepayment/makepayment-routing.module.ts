import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MakepaymentComponent } from './makepayment.component';

const routes: Routes = [
  {
    path: '', component: MakepaymentComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MakepaymentRoutingModule { }
