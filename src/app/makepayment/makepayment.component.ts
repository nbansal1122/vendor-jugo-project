import { AppUtil } from './../framework/Utils/AppUtil';
import { PaymentMode } from './../Model/GlobalEnum';
import { SubscriptionResponse } from './../Model/SubscriptionResponse';
import { Vendor } from './../Model/Loginrequest/LoginRequest';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PackageData, SubscriptionsData, PackageResponse, SubscriptionsResponse, MakePayment } from '../Model/Package/package';
import { CommonService } from '../framework/common.service';
import { Router } from '@angular/router';
import { BaseComponent } from '../framework/BaseCompo';
import { TaskCode } from '../framework/globals';
import { ApiGenerator } from '../framework/ApiGenerator';
import { PaymentResponse } from '../Model/PaymentResponse';
import { PAUSE } from '@angular/cdk/keycodes';
import { StorageUtil, StorageKeysEnum } from '../framework/StorageUtil';


@Component({
  selector: 'app-makepayment',
  templateUrl: './makepayment.component.html',
  styleUrls: ['./makepayment.component.scss']
})
export class MakepaymentComponent extends BaseComponent implements OnInit {

  package: PackageData;
  subcribe: Array<SubscriptionsData>;
  vendor: Vendor;
  id: String;
  subscriptionId: number;
  makePayment: MakePayment;


  paymentMode: string;
  paymentPackageId: number;
  amount: number;

  @Output() output = new EventEmitter<any>();
  constructor(private service: CommonService, private common: CommonService, router: Router) {
    super(common);
  }

  ngOnInit() {
    this.paymentMode = "Online";
    this.package = new PackageData();
    this.subcribe = new Array<SubscriptionsData>();
    this.getAllSubcribe();
    console.log(this.subscriptionId)
    this.showModal();
  }



  showModal() {
    const modal = document.getElementById('paymentModal');
    modal.style.display = "block";
  }

  closeModal() {
    const modal = document.getElementById('paymentModal');
    modal.style.display = "none";
  }


  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);

    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.ADD_ALL_PACKAGE:
          const packageres = response as PackageResponse;
          this.package = packageres.data;
          console.log(this.package);
          break;
        case TaskCode.GET_SUBSCRIPTION:
          const subcribeRes = response as SubscriptionsResponse;
          this.subcribe = subcribeRes.data;
          console.log(this.subcribe);
          break;
        case TaskCode.MAKE_PAYMENT:
          console.log(response);
          const paymentResponse = response as PaymentResponse;
          if (!AppUtil.isNullEmpty(paymentResponse)
            && !AppUtil.isNullEmpty(paymentResponse.data)
            && !AppUtil.isNullEmpty(paymentResponse.data.payment_request)
            && !AppUtil.isNullEmpty(paymentResponse.data.payment_request.longurl)) {
            if (this.paymentMode == PaymentMode.Online) {
              var payment_url = paymentResponse.data.payment_request.longurl;
              window.location.replace(payment_url);
            }
          }
          if (this.paymentMode == PaymentMode.Cash) {
            if (response.error == false) {
              this.close();
            }
            alert("Please wait for a while until admin approve your cash payment request");
          }
          break;
        case TaskCode.GET_MAX_VALUE_VENDOR_SUBSCRIPTION:
          const subscriptionResponse = response as SubscriptionResponse;
          if (!AppUtil.isNullEmpty(subscriptionResponse)
            && !AppUtil.isNullEmpty(subscriptionResponse.data)) {
            this.paymentPackageId = subscriptionResponse.data.paymentPackageId;
            this.amount = subscriptionResponse.data.amount;
          } else {
            this.amount = null;
          }
          break;
      }
    }
    return true;
  }

  onMakePaymentClick() {

    if (AppUtil.isNullEmpty(this.paymentPackageId)) {
      alert("Please select a package");
      return;
    }
    if (AppUtil.isNullEmpty(this.amount)) {
      alert("Could not find amount");
      return;
    }
    // if (AppUtil.isNullEmpty(this.paymentMode)) {
    //   alert("Please select Payment Type")
    // }

    console.log(this.paymentPackageId);

    this.makePayment = new MakePayment();
    this.makePayment.vendorId = +localStorage.getItem("vendorId");
    this.makePayment.paymentPackageId = this.paymentPackageId;

    console.log(this.makePayment.vendorId);

    if (this.paymentMode == PaymentMode.Online) {
      this.makePayment.online = true;
    } else {
      this.makePayment.online = false;
    }
    console.log(this.makePayment.online);
    this.makePaymentApi();

  }

  makePaymentApi() {
    console.log(this.makePayment);
    this.downloadData(ApiGenerator.makePayment(this.makePayment));
  }


  getAllPackage() {
    this.downloadData(ApiGenerator.addPackage(this.package));
  }
  getAllSubcribe() {
    this.downloadData(ApiGenerator.getSubscritption());
  }



  getAmount() {
    this.amount = null;
    let vendorId = +StorageUtil.getItemLocal(StorageKeysEnum.VENDOR_ID);
    if (AppUtil.isNullEmpty(vendorId)) {
      alert("Please try later")
      return false;
    }
    this.downloadData(ApiGenerator.getMaxValueSubscription(vendorId, this.subscriptionId));
  }





  close() {
    this.closeAndEmit();
  }


  closeAndEmit() {
    this.closeModal();
    this.output.emit();
  }




}
