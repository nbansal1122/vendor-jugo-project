import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../framework/BaseCompo';
import { CommonService } from '../../framework/common.service';
import { ResetVendorPassword } from '../../Model/vendor/vendor';
import { TaskCode } from '../../framework/globals';
import { Router } from '@angular/router';
import { ApiGenerator } from '../../framework/ApiGenerator';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent extends BaseComponent implements OnInit {

  resetPass:ResetVendorPassword;

  constructor(public commonService:CommonService, public router:Router) {
    super(commonService);
   }

  ngOnInit() {
    this.resetPass = new ResetVendorPassword();
    this.resetPass.mobileNumber = localStorage.getItem('vendorMobile');
  }



  onResponseReceived(taskCode:TaskCode, response:any){
    const isSuccess = super.onResponseReceived(taskCode, response)
    if(isSuccess){
      switch(taskCode){
        case TaskCode.RESET_PASSWORD:
        alert("login with new password");
        this.router.navigate(['./login']);
      }
    }
    return true;
  }


  resetPassword(){

    if(this.resetPass.newPassword !== null && this.resetPass.newPassword !== undefined){

      this.downloadData(ApiGenerator.resetVendorPassword(this.resetPass));
    }
  }



}
