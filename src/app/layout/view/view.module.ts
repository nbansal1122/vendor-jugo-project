import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{ FormsModule, ReactiveFormsModule} from '@angular/forms'
import { ViewRoutingModule } from './view-routing.module';
import { ViewComponent } from './view.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  imports: [
    CommonModule,
    ViewRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ViewComponent, ProfileComponent]
})
export class ViewModule { }
