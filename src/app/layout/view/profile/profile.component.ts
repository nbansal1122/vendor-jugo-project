import { Router } from '@angular/router';

import { DateUtil } from './../../../framework/Utils/DateUtil';
import { ApiGenerator } from './../../../framework/ApiGenerator';
import { TaskCode } from './../../../framework/globals';
import { ImageUrl, UplodePhotoResponse } from './../../../Model/vendor/vendor';

import { CommonService } from './../../../framework/common.service';
import { DesignData, UserData, UserResponse, Images, ImageResponse, DesignResponse, DesignDataResponse } from './../../../Model/event/event';
import { BaseComponent } from './../../../framework/BaseCompo';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { getLocaleDateTimeFormat } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends BaseComponent implements OnInit {

  @Input() profile: UserData;
  @Output() onUpdate: EventEmitter<any> = new EventEmitter<any>();
  @Input() eventTemplateId: number;
  date: Date;
  userDetail: UserData;
  isUpload: boolean;
  img: Images;
  photo: DesignData
  desId: string;
  isImageUpload: boolean;
  isSuperAdmin: boolean;
  headname: string;
  imageurl:string
  fileToUpload: File= null;


  constructor(private service: CommonService, private router: Router) {
    super(service)
  }

  ngOnInit() {
    this.showPopup();
    this.headname = 'Add'
    this.userDetail = new UserData();
    if (this.profile != null && this.profile != undefined) {
      this.headname = 'Edit'
    }
    this.isImageUpload = true;
    this.img = new Images();
    this.photo = new DesignData();
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        // case TaskCode.CREATE_EVENT:
        //   const eve = response as UserResponse;
        //   this.userDetail = eve.data;
        //   console.log(this.userDetail);
        //   this.closePopup();
        //   break;
        case TaskCode.ADD_IMAGE:
          // const userPhoto = response as ImageResponse;
          // if (userPhoto && userPhoto !== null && userPhoto !== undefined) {
          //   this.img = userPhoto.data;
          //   this.imgUrl(this.img)
          //   this.isUpload = false;
          // }
          // console.log(this.img);
          const userPhoto = response as ImageResponse;
          this.img = userPhoto.data;
          if (this.img) {
            this.imageUpload(this.img)
            this.isUpload = false;
          }
          console.log(this.img);
          break;
        case TaskCode.SAVE_IMAGE:
          const imageRes = response as DesignDataResponse
          this.photo = imageRes.data;
          this.closePopup();
          console.log("hello image is " + this.photo)

      }
    }
    return true;
  }

  // imgUrl(item) {
  //   this.img.imageurl= item;
  //   // this.photo.designImgUrl= this.img.imageurl
  //   this.downloadData(ApiGenerator.saveImage(this.photo))
  // }

  imageUpload(item) {
    this.photo.designId = this.photo.parentEventTemplateId
    // console.log("id is "+ this.photo.designId);
    this.photo.parentEventTemplateId = this.eventTemplateId;
    console.log("design id is " + this.photo.parentEventTemplateId);

    this.photo.designImgUrl = item;
    console.log("url is " + this.photo.designImgUrl);
    // this.downloadData(ApiGenerator.saveImage(this.photo));
  }

  showPopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'block';
  }


  closePopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'none';
    this.onUpdate.emit();
    this.profile = null;
    console.log(this.profile);
  }

  onFileSelected(file: FileList){
    this.fileToUpload = file.item(0)
    var reader = new FileReader();
    reader.onload=(event:any) =>{
      this.imageurl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload)

  }

  detectFiles(event: any) {
    this.isUpload = true;
    this.downloadData(ApiGenerator.getFilesRequests(event.target.files[0]));

  }



  onSave() {
    //   this.userDetail.dateTime = +DateUtil.getTimeStamp(this.date);
    //   console.log(this.userDetail.dateTime);
    // this.userDetail.designId = this.profile.designId;
    //   console.log("id is "+ this.designId);
    //   this.downloadData(ApiGenerator.addUserData(this.userDetail));  
    //   console.log(this.userDetail);
    //   alert("Event created successfully")
    //   this.router.navigate(['/events'])
    // }
    
      this.downloadData(ApiGenerator.saveImage(this.photo));
  }

}


