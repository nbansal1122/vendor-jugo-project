import { Component, OnInit } from '@angular/core';
import { CityData, CityResponse, CityStatusUpdate } from '../../Model/city/city';
import { ApiGenerator } from '../../framework/ApiGenerator';
import { BaseComponent } from '../../framework/BaseCompo';
import { CommonService } from '../../framework/common.service';
import { TaskCode } from '../../framework/globals';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent extends BaseComponent implements OnInit {

  popup: boolean;
  cityData: Array<CityData>;
  city: CityData;
  statusUpdate: CityStatusUpdate;

  constructor(public service: CommonService, ) {
    super(service)
  }

  ngOnInit() {
    this.cityData = new Array<CityData>();
    this.statusUpdate = new CityStatusUpdate();
    this.getAllCityData();
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);

    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.GET_ALL_CITY:
          const cityResponse = response as CityResponse;
          this.cityData = cityResponse.data;
          console.log(this.cityData);
          break;
        case TaskCode.UPDATE_CITY_STATUS:
          this.getAllCityData();
          break;
        // case TaskCode.DELETE_CATEGORY:
        // this.getAllCityData();  
      }
    }
    return true;
  }

  onUpdate(event) {
    this.popup = false;
    this.getAllCityData();
  }


  getAllCityData() {
    this.downloadData(ApiGenerator.getAllCity());
  }


  onAddClick() {
    this.popup = true;
    this.city = null;
    console.log(this.popup);
  }

  onEditCity(c: CityData) {
    this.popup = true;
    this.city = c;
  }

  onStatusUpdate(item: CityData) {
    this.statusUpdate.id = item.id;
    if (item.active === true) {
      this.statusUpdate.status = false;
    } else {
      this.statusUpdate.status = true;
    }
    this.downloadData(ApiGenerator.updateCityStatus(this.statusUpdate));
  }


}
