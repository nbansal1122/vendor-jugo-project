import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CityData, CityDataResponse } from '../../../Model/city/city';
import { TaskCode } from '../../../framework/globals';
import { CommonService } from '../../../framework/common.service';
import { ApiGenerator } from '../../../framework/ApiGenerator';
import { BaseComponent } from '../../../framework/BaseCompo';
import * as _ from 'lodash';

@Component({
  selector: 'app-addcitymodel',
  templateUrl: './addcitymodel.component.html',
  styleUrls: ['./addcitymodel.component.scss']
})
export class AddcitymodelComponent extends BaseComponent implements OnInit {

  @Input() city: CityData
  @Output() onUpdate: EventEmitter<any> = new EventEmitter<any>();
  popup: boolean;
  cityData: CityData;
  headName: string;

  constructor(private service: CommonService) {
    super(service);
  }

  ngOnInit() {

    this.showPopup();
    this.headName = "Add";
    this.cityData = new CityData();
    console.log(this.city);
    if (this.city !== null && this.city !== undefined) {
      this.cityData = _.clone(this.city);
      this.headName = 'Edit';
    }

  }


  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);

    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.ADD_CITY:
          console.log(JSON.stringify(response));
          const res = response as CityDataResponse;
          this.cityData = res.data;
          this.closePopup();
          console.log(this.closePopup());

          break;
        case TaskCode.UPDATE_CITY:
          console.log(JSON.stringify(response));

          const updateres = response as CityDataResponse;
          this.cityData = updateres.data;
          console.log(this.cityData);
          this.closePopup();
          break;
      }
    }
    return true;
  }

  getAddCityApi() {
    this.downloadData(ApiGenerator.addNewCity(this.cityData));
  }

  showPopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'block';
  }

  closePopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'none';
    this.onUpdate.emit();
    this.cityData = null;
    console.log(this.cityData);
  }

  getUpdateCityApi() {
    this.downloadData(ApiGenerator.updateCitydata(this.cityData, this.city));
  }

  onSaveClick() {
    if (this.cityData.id !== null && this.cityData.id !== undefined) {
      this.getUpdateCityApi();
    } else {
      this.getAddCityApi();
    }
  }


}
