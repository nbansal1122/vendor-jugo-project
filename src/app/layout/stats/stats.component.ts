import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { routerTransition } from '../../router.animations';
import { VendorData, VendorMasterResponse } from '../../Model/vendor/vendor';
import { TaskCode } from '../../framework/globals';
import { CommonService } from '../../framework/common.service';
import { BaseComponent } from '../../framework/BaseCompo';
import { ApiGenerator } from '../../framework/ApiGenerator';
import { CountResponse, CountType, UserCount, User, Pagination, CountTypeEnum } from '../../Model/userView/vendorCount';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageUtil, StorageKeysEnum } from '../../framework/StorageUtil';
import { AppUtil } from '../../framework/Utils/AppUtil';


@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
  animations: [routerTransition()]

})
export class StatsComponent extends BaseComponent implements OnInit {
  currentPage: number;
  size: number = 5;
  totalItems: number;
  vendordata: VendorData;
  userId: number;
  type: CountType;
  userCount: Array<UserCount>;
  user: UserCount;
  tableHead: string;
  showHide: boolean;
  srIndex: number;

  countTypeEnum = CountTypeEnum;
  constructor(public service: CommonService, public router: Router, public activateRoute: ActivatedRoute) {
    super(service);
  }

  ngOnInit() {
    this.type = new CountType();
    this.userCount = new Array<UserCount>();

    this.userId = this.activateRoute.snapshot.params.vendorId;
    if (!this.userId) {
      this.userId = +StorageUtil.getItemLocal(StorageKeysEnum.VENDOR_ID);
    }

    this.downloadData(ApiGenerator.getUserId(this.userId));
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.USER:
          const vendorResponse = response as VendorMasterResponse;
          this.vendordata = vendorResponse.data;
          console.log(this.vendordata)
          break;
        case TaskCode.GET_COUNTTYPE:
          // this.userCount = null;
          const countResponse = response as CountResponse;
          if (AppUtil.isEmpty(countResponse.data)) {
            alert("There are no details to be displayed for this category");
          }
          if (!AppUtil.isEmpty(countResponse.data)) {
            this.userCount = countResponse.data;
            this.totalItems = response.pagination.total;
            this.showHide = true;
            console.log(this.userCount);
          }
      }
    }
    return true;
  }



  getCountType(item: number) {
    this.showHide = false;
    this.type.id = +this.userId;
    this.currentPage = 1;

    if (item === 1) {
      this.tableHead = "Profile Visit";
      this.type.countType = this.countTypeEnum.PROFILE_VIEW_COUNT;
      console.log("page number is " + this.type.pageNo);
      this.downloadData(ApiGenerator.getCountType(this.type, this.currentPage, this.size));

    } else if (item === 2) {
      this.tableHead = "Calls";
      this.type.countType = this.countTypeEnum.PHONE_CONTACT_COUNT;
      this.downloadData(ApiGenerator.getCountType(this.type, this.currentPage, this.size));

    } else if (item === 3) {
      this.tableHead = "Web Visit";
      this.type.countType = this.countTypeEnum.EMAIL_CONTACT_COUNT;
      this.downloadData(ApiGenerator.getCountType(this.type, this.currentPage, this.size));

    } else if (item === 4) {
      this.tableHead = "Display";
      this.type.countType = this.countTypeEnum.DISPLAY_COUNT;
      this.downloadData(ApiGenerator.getCountType(this.type, this.currentPage, this.size));
    }
  }


  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.userCount = new Array<UserCount>();
    this.downloadData(ApiGenerator.getCountType(this.type, this.currentPage, this.size));
  }


  getSrIndex(i) {
    this.srIndex = (this.currentPage - 1) * this.size + 1;
    return this.srIndex + i;
  }

} 
