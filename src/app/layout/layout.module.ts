import { EventsComponent } from './events/events.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { StatsComponent } from './stats/stats.component';
import { PaymentsComponent } from './payments/payments.component';
import { ForgetComponent } from './forget/forget.component';
import { ResetComponent } from './reset/reset.component';



@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule.forRoot(),
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent ]
})
export class LayoutModule {}
