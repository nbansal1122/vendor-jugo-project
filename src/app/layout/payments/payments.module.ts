import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentsRoutingModule } from './payments-routing.module';
import { PaymentsComponent } from './payments.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MakepaymentModule } from '../../makepayment/makepayment.module';
import { PendingPaymentComponent } from './pending-payment/pending-payment.component';


@NgModule({
  imports: [
    CommonModule,
    PaymentsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MakepaymentModule
  ],
  declarations: [PaymentsComponent, PendingPaymentComponent]
})
export class PaymentsModule { }
