import { StorageKeysEnum } from './../../framework/StorageUtil';
import { CommonService } from './../../framework/common.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../framework/BaseCompo';
import { ApiGenerator } from '../../framework/ApiGenerator';
import { TaskCode } from '../../framework/globals';
import { AppUtil } from '../../framework/Utils/AppUtil';
import { PaymentInfo, PaymentHistoryResponse } from '../../Model/PaymentHistoryResponse';
import { StorageUtil } from '../../framework/StorageUtil';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent extends BaseComponent implements OnInit {

  makePayment: boolean;
  paymentInfo: Array<PaymentInfo>;

  showTable: boolean;
  constructor(public service: CommonService, public activated: ActivatedRoute) {
    super(service);
  }

  ngOnInit() {
    let vendorId = +StorageUtil.getItemLocal(StorageKeysEnum.VENDOR_ID);
    this.downloadData(ApiGenerator.getPaymentHistorybyVendorId(vendorId));
  }


  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.GET_PAYMENT_HISTORY:
          this.paymentInfo = null;
          const paymentResponse = response as PaymentHistoryResponse;
          if (!AppUtil.isNullEmpty(paymentResponse) && paymentResponse.data.length > 0 ) {
            this.paymentInfo = paymentResponse.data;
            this.showTable = true;
          }
          else
          {
          this.showTable = false;
          }
      }
    }
    return true;
  }


  onErrorReceived(taskCode: TaskCode, response: any) {
    switch (taskCode) {
      case TaskCode.GET_PAYMENT_HISTORY:
        console.log(response);
        alert("Could not get payment details due to some error");
    }
  }

  popupEvent()
  {
    this.makePayment = false;
    this.ngOnInit();
  }


  openPaymentModal()
  {
    this.makePayment = true;
  }


}
