import { PendingPaymentComponent } from './pending-payment/pending-payment.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentsComponent } from './payments.component';


const routes: Routes = [
  {path:'',component:PaymentsComponent},
  { path: 'payment-info/:paymentType', component: PendingPaymentComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
