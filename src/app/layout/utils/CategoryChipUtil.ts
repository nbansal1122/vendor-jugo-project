
import { MatAutocompleteSelectedEvent, MatChipInputEvent, throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { element } from 'protractor';
import { ElementRef, ViewChild } from '@angular/core';

export class CategoryChipUtil {

    static categories = new Array<any>();
    static categoriesId: number[] = [];

    @ViewChild('categoryInput')
    static categoryInput: ElementRef;
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = false;
    addOnBlur: boolean = false;
    separatorKeysCodes = [ENTER, COMMA];
    static categoryCtrl = new FormControl();
    filteredCategory: Observable<any[]>;


    static add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        // Add our fruit
        if ((value || '').trim()) {
            this.categories.push(value.trim());
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }

        this.categoryCtrl.setValue(null);
    }

    static remove(category: any): void {
        console.log(category);
        const index = this.categories.indexOf(category);

        if (index >= 0) {
            this.categories.splice(index, 1);
            console.log(this.categories);
            this.categoriesId.splice(index, 1);
            console.log(this.categoriesId);

        }
    }

    static filter(name: string) {
        console.log(this.categories)
        return this.categories.filter(fruit =>
            fruit.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    static selected(event: MatAutocompleteSelectedEvent): void {
        console.log(event);
        console.log(this.categories);
        if (this.categories.indexOf(event.option.value) == -1
            && this.categoriesId.indexOf(event.option.value.categoryId) == -1) {
            this.categories.push(event.option.value);
            this.categoriesId.push(event.option.value.categoryId);
            console.log(this.categories);
        }

        this.categoryInput.nativeElement.value = '';
        this.categoryCtrl.setValue(null);

    }
}