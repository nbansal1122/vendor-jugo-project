import { TaskCode } from './../../../framework/globals';
import { CommonService } from './../../../framework/common.service';
import { BaseComponent } from './../../../framework/BaseCompo';
import { ApiGenerator } from './../../../framework/ApiGenerator';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Images, ImageResponse } from '../../../Model/event/event';

@Component({
  selector: 'app-addnotificationmodel',
  templateUrl: './addnotificationmodel.component.html',
  styleUrls: ['./addnotificationmodel.component.scss']
})
export class AddnotificationmodelComponent extends BaseComponent implements OnInit {

  @Output() onUpdate: EventEmitter<any> = new EventEmitter<any>();
  isUpload:boolean
  fileToUpload: File=null;
  imageurl:string='';
  isImageUpload:boolean;
  img: Images;

  constructor(private service: CommonService) {
    super(service)
   }

  ngOnInit() {
    this.showPopup()
    this.isImageUpload = true;
    this.img = new Images();
  }

  onResponseReceived(taskcode: TaskCode, response: any){
    const isSuccess = super.onResponseReceived(taskcode,response);
    if(isSuccess){
      switch(taskcode){
        case TaskCode.ADD_IMAGE:
          const pic = response as ImageResponse;
          this.img = pic.data;
          if(this.img){
            this.isUpload = false;
          }
          break;
      }
    }
    return true;
  }

  showPopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'block';
  }

  closePopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'none';
    this.onUpdate.emit();
    
  }
  // onFileSelected(file: FileList){
  //   this.fileToUpload = file.item(0)
  //   var reader = new FileReader();
  //   reader.onload=(event:any) =>{
  //     this.imageurl = event.target.result;
  //   }
  //   reader.readAsDataURL(this.fileToUpload)

  // }
  detectFiles(event: any) {
    this.isUpload = true;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        // this.imageurl = event.target.result;
      }
    }
    this.downloadData(ApiGenerator.getFilesRequests(event.target.files[0]));

  }

  onSave(){}
}
