import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnotificationmodelComponent } from './addnotificationmodel.component';

describe('AddnotificationmodelComponent', () => {
  let component: AddnotificationmodelComponent;
  let fixture: ComponentFixture<AddnotificationmodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddnotificationmodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnotificationmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
