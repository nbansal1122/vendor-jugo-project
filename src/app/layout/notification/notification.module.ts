import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationComponent } from './notification.component';
import { AddnotificationmodelComponent } from './addnotificationmodel/addnotificationmodel.component';

@NgModule({
  imports: [
    CommonModule,
    NotificationRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [NotificationComponent, AddnotificationmodelComponent]
})
export class NotificationModule {
  constructor(){
    console.log("notifacition")
  }
 }
