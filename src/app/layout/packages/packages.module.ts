import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagesRoutingModule } from './packages-routing.module';
import { PackagesComponent } from './packages.component';
import { PackagesmodelComponent } from './packagesmodel/packagesmodel.component';
import {FormsModule,ReactiveFormsModule}from'@angular/forms';
import { DeletedpopupComponent } from './deletedpopup/deletedpopup.component';

@NgModule({
  imports: [
    CommonModule,
    PackagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    

  ],
  declarations: [PackagesComponent, PackagesmodelComponent, DeletedpopupComponent]
})
export class PackagesModule { }
