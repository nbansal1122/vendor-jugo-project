import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../framework/common.service';
import { Router } from '@angular/router';
import { BaseComponent } from '../../framework/BaseCompo';
import { Category, CategoryResponse } from '../../Model/vendor/vendor';
import { TaskCode } from '../../framework/globals';
import { ApiGenerator } from '../../framework/ApiGenerator';
import { SubscriptionsResponse, SubscriptionsData, PackageResponse, PackageData, PackageDetailResponse, PackageDetail } from '../../Model/Package/package';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent extends BaseComponent implements OnInit {
  popup: boolean;
  categoriesdata: Array<Category>;
  subcribe: Array<SubscriptionsData>;
  cat: Category;
  package: Array<PackageDetail>;
  popup2: boolean;
  packageData: PackageDetail;
  isDelete: boolean;



  constructor(private service: CommonService, private common: CommonService, router: Router) {
    super(common);
  }


  ngOnInit() {
    this.getAllCategory();
    this.getAllSubcribe();
    this.getAllPackage();
    this.categoriesdata = new Array<Category>();
    this.cat = new Category();
    this.subcribe = new Array<SubscriptionsData>();
    this.package = new Array<PackageDetail>();

  }

  // onDelete(item: PackageDetail) {
  //   this.popup2 = true;
  //   //  this.packageDetail =  item;
  //   this.packageData = item;
  // }


  onAdd() {
    this.popup = true;
  }

  onEditCategory(item: PackageDetail) {
    this.popup = true;
    this.packageData = item;

  }


  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);

    if (isSuccess) {
      switch (taskCode) {

        case TaskCode.GET_ALL_CATEGORY:
          const categoriesResponse = response as CategoryResponse;
          this.categoriesdata = categoriesResponse.data;
          console.log(this.categoriesdata);
          break;
        case TaskCode.GET_SUBSCRIPTION:
          const subcriberes = response as SubscriptionsResponse;
          this.subcribe = subcriberes.data;
          console.log(this.subcribe);
          break;
        case TaskCode.ADD_ALL_PACKAGE:
          const packageres = response as PackageDetailResponse;
          this.package = packageres.data;
          console.log(this.package);
          break;
        case TaskCode.DELETE_PACKAGE:
          const packagedel = response as PackageDetailResponse;
          this.getAllPackage();
          console.log(packagedel);
          break;
      }
    }
    return true;
  }
  getAllCategory() {
    this.downloadData(ApiGenerator.getAllCategory())
  }
  getAllSubcribe() {
    this.downloadData(ApiGenerator.getSubscritption())
  }
  getAllPackage() {
    this.downloadData(ApiGenerator.getPackage())
  }

  onUpdate(event) {
    this.popup = false;
    this.packageData = new PackageDetail();
    this.getAllPackage();
  }

  onDeletePackage(item:PackageDetail) {
    let isOk = confirm("Are you sure want to delete package !");
    if (isOk) {
      this.downloadData(ApiGenerator.deletePackage(item));
    }
  }

}
