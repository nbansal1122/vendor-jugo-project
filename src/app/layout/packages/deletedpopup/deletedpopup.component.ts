import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-deletedpopup',
  templateUrl: './deletedpopup.component.html',
  styleUrls: ['./deletedpopup.component.scss']
})
export class DeletedpopupComponent implements OnInit {
  popup: boolean;
  // @Output() onDeletePackage:EventEmitter<any>= new EventEmitter<any>();
  @Output() onDeletePackage: EventEmitter<any> = new EventEmitter<any>();

  isDelete:boolean;
  constructor() { }

  ngOnInit() {
    this.showPopup();

  }
  showPopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'block';
  }

  closePopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'none';
    this.onDeletePackage.emit(this.isDelete);
  }

  deletePackage(){
    this.isDelete= true;
    this.closePopup();
    this.onDeletePackage.emit(this.isDelete);

    // this.onDeletePackage.emit(this.isDelete);
  }

  ondelete(event) {
    this.popup = false;
  }

}
