import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletedpopupComponent } from './deletedpopup.component';

describe('DeletedpopupComponent', () => {
  let component: DeletedpopupComponent;
  let fixture: ComponentFixture<DeletedpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletedpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletedpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
