import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackagesmodelComponent } from './packagesmodel.component';

describe('PackagesmodelComponent', () => {
  let component: PackagesmodelComponent;
  let fixture: ComponentFixture<PackagesmodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackagesmodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackagesmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
