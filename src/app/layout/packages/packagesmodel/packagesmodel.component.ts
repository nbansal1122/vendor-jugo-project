import { PackageDetail } from './../../../Model/Package/package';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../../../framework/BaseCompo';
import { CommonService } from '../../../framework/common.service';
import { Router } from '@angular/router';
import { Category, CategoryResponse, CategoryResponsebyid } from '../../../Model/vendor/vendor';
import { ApiGenerator } from '../../../framework/ApiGenerator';
import { TaskCode } from '../../../framework/globals';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubscriptionsData, PackageData, PackageResponse } from '../../../Model/Package/package';
import * as _ from 'lodash';

@Component({
  selector: 'app-packagesmodel',
  templateUrl: './packagesmodel.component.html',
  styleUrls: ['./packagesmodel.component.scss']
})
export class PackagesmodelComponent extends BaseComponent implements OnInit {
  @Input() category: Category;
  @Input() package: PackageDetail;
  @Input() subcribe: SubscriptionsData;
  @Output() onUpdate: EventEmitter<any> = new EventEmitter<any>();

  categoriesdata: Array<Category>;
  cat: Category;
  packageData: PackageData;
  popup: boolean;
  constructor(private service: CommonService, private common: CommonService, router: Router) {
    super(common);
  }

  ngOnInit() {
    this.showPopup();
    this.packageData = new PackageData();

    if (this.package !== null && this.package !== undefined) {
      this.packageData = _.clone(this.package);
    }

    // this.categoriesdata = new Array<Category>();

    // this.getAllCategory();
    // this.cat = new Category();
  }



  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);

    if (isSuccess) {
      switch (taskCode) {

        case TaskCode.ADD_ALL_PACKAGE:
          const packageres = response as PackageResponse;
          this.packageData = packageres.data;
          console.log(this.packageData);
          this.closePopup();
          break;
        case TaskCode.UPDATE_PACKAGE:
          const packageres1 = response as PackageResponse;
          this.packageData = packageres1.data;
          console.log(this.packageData);
          this.closePopup();
          break;
      }
    }
    return true;
  }


  getAllPackage() {
    this.downloadData(ApiGenerator.addPackage(this.package))
  }

  showPopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'block';
  }

  closePopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'none';
    this.package = null;
    this.onUpdate.emit();
  }



  onSave() {

    if (this.packageData.paymentPackageId !== null && this.packageData.paymentPackageId !== undefined) {
      this.downloadData(ApiGenerator.updatePackage(this.packageData, this.package));
      console.log("packageUpdate");
    } else {
      console.log("packageAdd");
      this.downloadData(ApiGenerator.addPackage(this.packageData));
    }
  }

  getAllCategory() {
    this.downloadData(ApiGenerator.getCategory(this.cat))
  }

}
