import { BaseComponent } from './../../framework/BaseCompo';
import { ApiGenerator } from './../../framework/ApiGenerator';
import { CommonService } from './../../framework/common.service';
import { TaskCode, FORGOT_PASSWORD } from './../../framework/globals';
import { ForgotPassword, ForgotPasswordResponse } from './../../Model/Loginrequest/LoginRequest';
import { AppUtil } from './../../framework/Utils/AppUtil';
import { Component, OnInit } from '@angular/core';
// import * as firebase from 'firebase';
import { WindowService } from '../window.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.scss']
})
export class ForgetComponent extends BaseComponent implements OnInit {

  forgotPassword: ForgotPassword;
  vendorEmail: string;
  constructor( public router: Router, public service: CommonService) {
    super(service);
  }

  ngOnInit() {

    this.forgotPassword = new ForgotPassword();
  }

  getForgetPasswordApi() {
    this.downloadData(ApiGenerator.postForgetPassword(this.forgotPassword));
  }

  onResponseReceived(taskcode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskcode, response);

    if (isSuccess) {
      switch (taskcode) {

        case TaskCode.FORGOT_PASSWORD:
          const passwordres = response as ForgotPasswordResponse;
          this.forgotPassword = passwordres.data;
          if (passwordres.error == false) {
            alert("Password has been sent to your Email Address");
            this.router.navigate(['/login']);
          }
          break;
      }
    }
    return true
  }

  sendPassword() {
    if (!AppUtil.isNullEmpty(this.vendorEmail)) {
      this.forgotPassword.emailId = this.vendorEmail;
      this.getForgetPasswordApi();
    }
    return
  }



}
