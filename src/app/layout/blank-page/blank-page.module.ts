import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarRatingModule } from "ngx-bar-rating";
import { BlankPageRoutingModule } from './blank-page-routing.module';
import { BlankPageComponent } from './blank-page.component';
import { EditprofilemodelComponent } from './editprofilemodel/editprofilemodel.component';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';


import { MatAutocompleteModule,MatChipsModule, MatIconModule, MatFormFieldModule} from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// import { NgbRating } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [CommonModule, BlankPageRoutingModule, 
    BarRatingModule,FormsModule,ReactiveFormsModule,MatChipsModule,
    MatIconModule,
    MatIconModule, MatAutocompleteModule, MatFormFieldModule,
    NgbModule.forRoot()],
    declarations: [BlankPageComponent, EditprofilemodelComponent],
    exports:[]
})
export class BlankPageModule {
   
}
