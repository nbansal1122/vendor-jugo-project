import { AppUtil } from './../../framework/Utils/AppUtil';
import { StorageKeysEnum } from './../../framework/StorageUtil';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BarRatingModule } from "ngx-bar-rating";
import { FormModule } from '../form/form.module';
import { VendorData, VendorResponse, ImageUrl, Photo, UplodePhotoResponse, PhotoData, VendorMasterResponse, VendorCategory, Category, CategoryResponse } from '../../Model/vendor/vendor';
import { BaseComponent } from '../../framework/BaseCompo';
import { CommonService } from '../../framework/common.service';
import { ApiGenerator } from '../../framework/ApiGenerator';
import { TaskCode } from '../../framework/globals';

import { MatAutocompleteSelectedEvent, MatChipInputEvent, throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { element } from 'protractor';
import { Router, ActivatedRoute } from '@angular/router';
import { Categories } from '../../Model/registration/RegisterRequest';
import { StorageUtil } from '../../framework/StorageUtil';

@Component({
  selector: 'app-blank-page',
  templateUrl: './blank-page.component.html',
  styleUrls: ['./blank-page.component.scss'],

})


export class BlankPageComponent extends BaseComponent implements OnInit {
  urls = new Array<string>();
  popup: boolean;
  onchange: boolean;
  userid: number;
  vendorData: VendorData;
  currentVendor: VendorData;
  imageUrl: ImageUrl;
  photo: PhotoData;
  photos: Array<Photo>;
  disable: boolean = false;
  currentRate = 3.14;

  isImageUpload: boolean;




  @ViewChild('categoryInput') categoryInput: ElementRef;
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = false;
  addOnBlur: boolean = false;
  separatorKeysCodes = [ENTER, COMMA];
  categoryCtrl = new FormControl();
  filteredCategory: Observable<any[]>;
  isCatSelected: boolean;
  categories = new Array<Category>();
  vendorCat: VendorCategory;
  categoriesdata: Array<Category>;
  isSuperAdmin: boolean;
  vendorRole: string;
  categoryhide: boolean;


  constructor(private common: CommonService, public router: Router,
    public activateRoute: ActivatedRoute) {
    super(common);
  }

  ngOnInit() {
    this.isSuperAdmin = false;
    this.isImageUpload = false;
    this.userid = this.activateRoute.snapshot.params.vendorId;
    if (!this.userid) {
      this.userid = +StorageUtil.getItemLocal(StorageKeysEnum.VENDOR_ID);
    }
    this.vendorData = new VendorData();
    this.photo = new PhotoData();
    this.vendorCat = new VendorCategory();
    this.categoriesdata = new Array<Category>();
    this.vendorRole = StorageUtil.getRole()
    this.getVendorById();
    this.getAllCategory();
    this.isValidAdmin();
  }

  isValidAdmin() {
    if (this.vendorRole !== 'Superadmin') {
      this.isSuperAdmin = true;
    }
  }

  getVendorById() {
    this.downloadData(ApiGenerator.getUserId(this.userid));
  }
  onEdittext() {
    this.onchange = true;
  }

  onEdit(vendor: VendorData) {
    this.currentVendor = vendor;
    this.popup = true;
  }
  onUpdate(event) {
    this.popup = false;
    this.getVendorById();

  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.USER:
          const vendorResponse = response as VendorMasterResponse;
          this.vendorData = vendorResponse.data;
          if (!AppUtil.isListNullOrEmpty(this.vendorData.categoryList)) {
            this.getVendorCategory(this.vendorData.categoryList);
            console.log(this.vendorData.categoryList);
          }
          this.isImageUpload = false;
          break;
        case TaskCode.UPLOAD_PHOTO:
          const vendorPhoto = response as UplodePhotoResponse;
          this.imageUrl = vendorPhoto.data;
          if (this.imageUrl) {
            this.imageUpload(this.imageUrl)
          }
          console.log(this.imageUrl);
          break;
        case TaskCode.UPLOAD_IMAGE:
          const imageResponse = response as VendorMasterResponse;
          this.vendorData = imageResponse.data
          if (this.vendorData) {
            this.downloadData(ApiGenerator.getUserId(this.userid));
          }
          console.log("image response::" + this.vendorData);
          break;
        case TaskCode.GET_ALL_CATEGORY:
          const categoriesResponse = response as CategoryResponse;
          this.categoriesdata = categoriesResponse.data;
          console.log(this.categoriesdata);
          break;
        case TaskCode.ADD_CATEGORY:
          const cat = response as CategoryResponse;
          this.getVendorById();

          break;
        case TaskCode.DELETE_VENDOR_IMAGE:
          this.getVendorById();
          console.log(this.getVendorById());

          break;
      }
    }
    return true;
  }

  onEditArea() {
    this.disable = !this.disable;

  }
  onEditCategory() {
    this.categoryhide = !this.categoryhide;
    this.removable = !this.removable;
  }

  getVendorCategory(categories: Array<Category>) {
    this.categories = categories;
  }

  remove(category: Category): void {
    this.categories.splice(this.categories.indexOf(category), 1);
  }

  filter(name: string) {
    console.log(this.categories)
    return this.categories.filter(ele =>
      ele.categoryName.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    console.log(event);
    console.log(this.categories);
    if (this.categories.indexOf(event.option.value) == -1) {
      this.categories.push(event.option.value);
    }
  }
  onUpdateArea() {
    this.vendorData;
    this.downloadData(ApiGenerator.updateProfile(this.vendorData));
    console.log();
    this.onEditArea();
  }

  detectFiles(event: any) {
    this.isImageUpload = true;
    this.downloadData(ApiGenerator.getFileRequest(event.target.files[0]));
    console.log("update Request : " + this.vendorData);
  }

  imageUpload(item) {
    this.photo.iconUrl = item;
    this.photo.vendorId = this.userid;
    this.downloadData(ApiGenerator.uploadImage(this.photo));
  }

  getAllCategory() {
    this.downloadData(ApiGenerator.getAllCategory())
  }

  updateCategory() {
    if (AppUtil.isListNullOrEmpty(this.categories)) {
      alert("Please add at least one category");
    } else {
      let categoriesId = [];
      for (let i = 0; i < this.categories.length; i++) {
        categoriesId.push(this.categories[i].categoryId)
      }
      this.vendorCat.vendorId = +this.userid;
      this.vendorCat.cetegoryId = categoriesId;
      this.downloadData(ApiGenerator.addCategory(this.vendorCat));
      this.removable = !this.removable;
      this.categoryhide = false;
    }
  }


  onDeleteClicked(photo: Photo) {
    let isOk = confirm("Are you sure want to delete this image");
    if (isOk) {
      this.downloadData(ApiGenerator.deleteVendorImage(photo));
    }
  }







}
