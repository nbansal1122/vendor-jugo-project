import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'
import { BaseComponent } from '../../../framework/BaseCompo';
import { CommonService } from '../../../framework/common.service';
import { ApiGenerator } from '../../../framework/ApiGenerator';
import { TaskCode } from '../../../framework/globals';
import { VendorData, VendorMasterResponse, VendorCategory, Category, CategoryResponse } from '../../../Model/vendor/vendor';



@Component({
  selector: 'app-editprofilemodel',
  templateUrl: './editprofilemodel.component.html',
  styleUrls: ['./editprofilemodel.component.scss']
})
export class EditprofilemodelComponent extends BaseComponent implements OnInit {
  @Input() currentVendor: VendorData;
  @Output() output:EventEmitter<any> = new EventEmitter<any>();
  vendorData: VendorData;
   userid: string;

  constructor(private common: CommonService) {
    super(common);
  }

  ngOnInit() {
    this.vendorData = new VendorData();
    this.showPopup()
    console.log(this.currentVendor);

    this.userid = localStorage.getItem("id");
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.UPDATE_PROFILE:
          const res = response as VendorMasterResponse;
          this.vendorData = res.data;
          console.log(this.vendorData);
          this.closePopup();
          break;
      }
    }
    return true;
  }

  showPopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'block';
  }

  closePopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'none';
    this.output.emit();
  }

  onProfileUpdate() {
    this.downloadData(ApiGenerator.updateProfile(this.currentVendor));
  }


}
