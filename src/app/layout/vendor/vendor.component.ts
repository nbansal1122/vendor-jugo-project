import { Component, OnInit } from '@angular/core';
import { VendorData, VendorResponse } from '../../Model/vendor/vendor';
import { TaskCode } from '../../framework/globals';
import { BaseComponent } from '../../framework/BaseCompo';
import { CommonService } from '../../framework/common.service';
import { ApiGenerator } from '../../framework/ApiGenerator';
import { log } from 'util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.scss']
})
export class VendorComponent extends BaseComponent implements OnInit {
  vendorDetails: Array<VendorData>;
  vendordata:VendorData;
  changecolor:boolean=false;

  constructor(private common: CommonService,public router:Router) {
    super(common);
  }

  ngOnInit() {
    this.vendorDetails = new Array<VendorData>();
    this.downloadData(ApiGenerator.getAllVendor());
    this.changecolor=!this.changecolor;

    
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.GET_ALL_VENDOR:
          const vendorResponse = response as VendorResponse;
          this.vendorDetails = vendorResponse.data;
          console.log("vendor details::" + vendorResponse.data)
          console.log(this.vendorDetails);
          break;
        case TaskCode.UPDATE_STATUS:
          const status = response as VendorResponse;
          this.downloadData(ApiGenerator.getAllVendor());
          break;
      }
    }
    return true;
  }

  updateStatus(value: VendorData) {
   
    if (value.status == "Inactive") {
      value.status = "ACTIVE";
      this.downloadData(ApiGenerator.updateStatus(value));
      console.log("value of status "+value.status);
    }
    else {
      value.status = "INACTIVE";
      this.downloadData(ApiGenerator.updateStatus(value));
    }
  }

  navigateProfile(vendor:VendorData){
    const vendorId = vendor.id;
    this.router.navigate(['./profile', vendorId]);
  }
  navigateStats(vendor:VendorData){
    const vendorId = vendor.id;
    this.router.navigate(['./stats', vendorId])
  }
  chcolor(){
  };
  
}
