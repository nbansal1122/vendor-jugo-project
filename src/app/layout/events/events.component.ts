import { EventData, EventDataResponse } from './../../Model/event/event';
import { Router } from '@angular/router';
import { TaskCode } from './../../framework/globals';
import { ApiGenerator } from './../../framework/ApiGenerator';


import { BaseComponent } from './../../framework/BaseCompo';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../framework/common.service';
import { DesignData } from '../../Model/event/event';
import { BaseResponse } from '../../framework/BaseResponseModel';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent extends BaseComponent implements OnInit {

  eventData: Array<EventData>;
  design: Array<DesignData>;
  constructor(private service: CommonService, private router: Router) {
    super(service)
   }

  ngOnInit() {
    this.getAllEvent();
    this.eventData = new Array<EventData>();
    this.design = new Array<DesignData>();
  }

  onResponseReceived(taskcode: TaskCode, response:any){
    const isSuccess= super.onResponseReceived(taskcode, response);
    if(isSuccess){
      switch(taskcode){
        case TaskCode.GET_ALL_EVENT_TEMPLATE:
          const eventResponse = response as EventDataResponse
          this.eventData =eventResponse.data;
          console.log(this.eventData);
          break;
          case TaskCode.DELETE_EVENT_TEMPLATE:
          const eventDeleteResponse = response as BaseResponse;
          if(!eventDeleteResponse.error){
            alert(eventDeleteResponse.message);
            this.ngOnInit();
          }else{
            alert(eventDeleteResponse.message);
          }
          
          break;
        
      }
    }
    return true;
  }

  getAllEvent(){
    this.downloadData(ApiGenerator.getAllEvent());
  }

  onView(event: EventData){
    const eventId = event.eventTemplateId;
    //console.log(eventId)
    this.router.navigate(['/view', eventId])
  }

  onDelete(event: EventData){
    if (confirm("You want to delete this event category?") == false) {
      return;
    }else{
      this.deleteEventCategory(event);
    }
  }

  deleteEventCategory(event: EventData){
    this.downloadData(ApiGenerator.deleteEventTemplate(event.eventTemplateId));
  }   
}
