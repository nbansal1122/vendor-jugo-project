import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CommonService } from '../../../framework/common.service';
import { Router } from '@angular/router';
import { BaseComponent } from '../../../framework/BaseCompo';
import { ApiGenerator } from '../../../framework/ApiGenerator';
import { TaskCode } from '../../../framework/globals';
import { UplodePhotoResponse, CategoryResponse, ImageUrl, VendorData, Category } from '../../../Model/vendor/vendor';
import { CategoryData, Categorydataresponse } from '../../../Model/category/category';
import * as _ from 'lodash';

@Component({
  selector: 'app-addcategorymodel',
  templateUrl: './addcategorymodel.component.html',
  styleUrls: ['./addcategorymodel.component.scss']
})
export class AddcategorymodelComponent extends BaseComponent implements OnInit {

  @Input() category: Category;
  @Output() onUpdate: EventEmitter<any> = new EventEmitter<any>();
  popup: boolean;
  categorydata: CategoryData;
  imageurl: ImageUrl;

  btnDisable: boolean;
  isUpload: boolean;
  headName: string;
  constructor(private service: CommonService, private common: CommonService, router: Router) {
    super(common);
  }

  ngOnInit() {
    this.showPopup();
    this.headName = 'Add';
    this.categorydata = new Category();
    console.log(this.category);
    if (this.category !== null && this.category !== undefined) {
      this.categorydata = _.clone(this.category);
      this.btnDisable = true;
      this.headName = 'Edit';
    }

  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.UPLOAD_PHOTO:
          const vendorPhoto = response as UplodePhotoResponse;
          if (vendorPhoto && vendorPhoto !== null && vendorPhoto !== undefined) {
            this.imageurl = vendorPhoto.data;
            this.imageUrl(this.imageurl)
            // this.btnDisable = false;
            this.isUpload = false;
          }
          console.log(this.imageurl);
          break;
        case TaskCode.ADD_ALL_CATEGORY:
          const cat = response as Categorydataresponse;
          this.categorydata = cat.data;
          console.log(this.categorydata);
          this.closePopup();
          break;
        case TaskCode.UPDATE_CATEGORY:
          const updatecat = response as Categorydataresponse;
          this.categorydata = updatecat.data;
          console.log(this.categorydata);
          this.closePopup();
          break;
      }
    }
    return true;
  }

  detectFiles(event: any) {
    this.isUpload = true;
    this.downloadData(ApiGenerator.getFileRequest(event.target.files[0]));
  }

  showPopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'block';
  }

  closePopup() {
    var model = document.getElementById('myModal');
    model.style.display = 'none';
    this.onUpdate.emit();
    this.category = null;
    console.log(this.category);
  }


  imageUrl(item) {
    this.categorydata.url = item;
  }

  onSave() {
    this.categorydata.categoryName;
    this.categorydata.description = "view";

    if (this.categorydata.categoryId !== null && this.categorydata.categoryId !== undefined) {
      this.downloadData(ApiGenerator.updateCategorydata(this.categorydata, this.category));
    } else {
      this.downloadData(ApiGenerator.addCategorydata(this.categorydata));
    }
  }

}
