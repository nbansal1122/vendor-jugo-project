import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcategorymodelComponent } from './addcategorymodel.component';

describe('AddcategorymodelComponent', () => {
  let component: AddcategorymodelComponent;
  let fixture: ComponentFixture<AddcategorymodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcategorymodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcategorymodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
