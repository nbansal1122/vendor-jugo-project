import { Component, OnInit } from '@angular/core';
import { TaskCode } from '../../framework/globals';
import { BaseComponent } from '../../framework/BaseCompo';
import { CommonService } from '../../framework/common.service';
import { Router } from '@angular/router';
import { CategoryResponse, Category, CategoryStatusUpdate } from '../../Model/vendor/vendor';
import { ApiGenerator } from '../../framework/ApiGenerator';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent extends BaseComponent implements OnInit {
  popup: boolean;
  categoriesdata: Array<Category>;
  category: Category;
  statusUpdate: CategoryStatusUpdate;


  constructor(private service: CommonService, private common: CommonService, router: Router) {
    super(common);
  }

  ngOnInit() {
    this.getAllCategory();
    this.categoriesdata = new Array<Category>();
    this.statusUpdate = new CategoryStatusUpdate();

  }
  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.GET_ALL_CATEGORY:
          const categoriesResponse = response as CategoryResponse;
          this.categoriesdata = categoriesResponse.data;
          console.log(this.categoriesdata);
          break;
        case TaskCode.DELETE_CATEGORY:
          this.getAllCategory();
          break;
        case TaskCode.UPDATE_CATEGORY_STATUS:
          this.getAllCategory();
          break;
      }
    }
    return true;
  }
  onAdd() {
    this.popup = true;
    this.category = null;
  }

  onEditCategory(item: Category) {
    this.popup = true;
    this.category = item;
  }

  onDltCategory(category: Category) {
    let isOk = confirm('Are you sure want to delete this category')
    console.log(isOk);
    if (isOk) {
      this.downloadData(ApiGenerator.deleteCategory(category));
    }
  }

  getAllCategory() {
    this.downloadData(ApiGenerator.getAllCategory());
  }
  onUpdate(event) {
    this.popup = false;
    this.getAllCategory();
  }

  onStatusUpdate(item: Category) {
    this.statusUpdate.id = item.categoryId;
    if (item.active === true) {
      this.statusUpdate.status = false;
    } else {
      this.statusUpdate.status = true;
    }
    this.downloadData(ApiGenerator.updateCategoryStatus(this.statusUpdate));
  }



  
}
