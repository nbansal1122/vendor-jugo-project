import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule,ReactiveFormsModule} from'@angular/forms';
import { CategoryRoutingModule } from './category-routing.module';
import {CategoryComponent} from './category.component';
import { AddcategorymodelComponent } from './addcategorymodel/addcategorymodel.component';
@NgModule({
  imports: [
    CommonModule,
    CategoryRoutingModule,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [CategoryComponent, AddcategorymodelComponent]
})
export class CategoryModule { }
