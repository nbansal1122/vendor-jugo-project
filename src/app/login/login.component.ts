import { StorageKeysEnum } from './../framework/StorageUtil';
import { AppUtil } from './../framework/Utils/AppUtil';
import { RoleEnum } from './../Model/GlobalEnum';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { Login, LoginResponse, Vendor } from '../Model/Loginrequest/LoginRequest';
import { BaseComponent } from '../framework/BaseCompo';
import { CommonService } from '../framework/common.service';
import { ApiGenerator } from '../framework/ApiGenerator';
import { TaskCode } from '../framework/globals';
import { CommonDataService } from '../shared/sharedDataService';
import { StorageUtil } from '../framework/StorageUtil';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()]
})
export class LoginComponent extends BaseComponent implements OnInit {
  login: Login;
  vendor: Vendor;
  vendorFullName: string;

  showPaymentModal: boolean;

  constructor(public router: Router, private service: CommonService) {
    super(service);
  }

  ngOnInit() {
    this.login = new Login();




  }

  onLoggedin() {
    localStorage.setItem('isLoggedin', 'true'); sessionStorage.clear();
    if (sessionStorage.getItem('id') !== undefined) {
      this.router.navigate(['./login']);
    }
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.LOGIN:
          console.log(response);
          const loginResponse = response as LoginResponse;
          if (!AppUtil.isNullEmpty(loginResponse) && !AppUtil.isNullEmpty(loginResponse.data)) {
            this.vendor = loginResponse.data;
          }
          if (!AppUtil.isNullEmpty(this.vendor) && !AppUtil.isNullEmpty(this.vendor.id)) {
            StorageUtil.setItemLocal(StorageKeysEnum.VENDOR_ID, this.vendor.id.toString()); 
            StorageUtil.setItemLocal(StorageKeysEnum.TOKEN, this.vendor.token.toString());
          }
          this.vendor.id = +StorageUtil.getId();
          StorageUtil.setItemLocal(StorageKeysEnum.VENDOR_ROLE, this.vendor.role);
          this.vendor.role = StorageUtil.getUserRole();
          localStorage.setItem("paymentPending", this.vendor.paymentPending.toString());
          if (this.vendor.paymentPending === true) {
            if (this.vendor.role !== RoleEnum.Superadmin) {
              this.showPaymentModal = true;
            }
            if (this.vendor.role == RoleEnum.Superadmin) {

              this.setLoggedInStorage();
              this.router.navigate(["/profile",this.vendor.id]);
            }
          }
          else {
            this.setLoggedInStorage();
            this.router.navigate(["/profile",this.vendor.id]);
          }
          break;
      }
    }
    return true;
  }

  popupEvent(event) {
    this.showPaymentModal = false;
  }

  setLoggedInStorage() {
    localStorage.setItem("isLoggedin", "true");
  }

  onLogin() {
    if (this.login.isValid) {
      console.log();
      
      this.showPaymentModal = false;
      this.downloadData(ApiGenerator.postLogin(this.login));
    }
  }

  onErrorReceived(taskCode: TaskCode, response: any) {
    console.log(response);
  }

}
