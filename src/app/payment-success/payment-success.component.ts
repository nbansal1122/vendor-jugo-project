import { AppUtil } from './../framework/Utils/AppUtil';
import { TaskCode } from './../framework/globals';
import { CommonService } from './../framework/common.service';
import { ApiGenerator } from './../framework/ApiGenerator';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../framework/BaseCompo';
import { PaymentInfo, PaymentHistoryResponse, PaymentTransactionResponse } from '../Model/PaymentHistoryResponse';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.scss']
})
export class PaymentSuccessComponent extends BaseComponent implements OnInit {

  paymentId: string;
  payment_requestId: string;

  paymentSuccess: boolean;
  showResponse: boolean;

  constructor(public service: CommonService, public router: Router, public activated: ActivatedRoute) {
    super(service);
  }

  ngOnInit() {

    this.paymentId = this.activated.snapshot.queryParams["payment_id"];
    this.payment_requestId = this.activated.snapshot.queryParams["payment_request_id"];
    this.downloadData(ApiGenerator.verifyOnlinePayment(this.payment_requestId));
  }


  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);

    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.VERIFY_ONLINE_PAYMENT:
          const res = response as PaymentTransactionResponse;
          if (AppUtil.isNullEmpty(res) || AppUtil.isNullEmpty(res.data)) {
            this.paymentSuccess = false;
          } else if (res.data.status == 'Credit') {
            this.paymentSuccess = true;
          } else {
            this.paymentSuccess = false;
          }
          this.showResponse = true;
          break;
      }
    }
    return true;
  }

  goBack() {
    let loginStatus = localStorage.getItem('isLoggedIn');
    if (loginStatus == 'false') {
      this.router.navigate(['login']);
    } else {
      this.router.navigate(['profile']);
    }
  }








}
