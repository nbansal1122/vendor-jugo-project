import { PaymentSuccessModule } from './payment-success.module';

describe('PaymentSuccessModule', () => {
  let paymentSuccessModule: PaymentSuccessModule;

  beforeEach(() => {
    paymentSuccessModule = new PaymentSuccessModule();
  });

  it('should create an instance', () => {
    expect(paymentSuccessModule).toBeTruthy();
  });
});
